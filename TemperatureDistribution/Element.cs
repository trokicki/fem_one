﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureDistribution {
	public struct Element {
		public uint PointA { get; set; }
		public uint PointB { get; set; }
		public double Conduction { get; set; }
		public double Convection { get; set; }
		public double Surface { get; set; }
		public double Length { get; set; }
		public double ExternalHeat { get; set; }

		public string ToString() {
			return String.Format("PointA= {0}; PointB= {1}; L= {2}", PointA, PointB, Math.Round(Length, 2));
		}
	}
}
