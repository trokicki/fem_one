﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureDistribution {
	public enum SolveMethod { SAFE_CODE, UNSAFE_CODE };

	interface ITemperatureDistribution {
		void Solve(SolveMethod solveMethod);
		double[] Temperatures { get; }
	}
}
