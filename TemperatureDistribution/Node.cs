﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureDistribution {
	public struct Node {
		public double X;
		public double Y;
		public double Z;
		public double Temperature;
		public BoundaryCondition BoundaryCondition;
		public uint ID;

		public string ToString() {
			return String.Format("X{0}= {1}; T{0}= {2}", ID, Math.Round(X, 2), Temperature);
		}
	}
}
