﻿using MatrixOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureDistribution {
	public class RadialTemperatureDistribution : ITemperatureDistribution {
		public double RadiusMin { get; set; }
		public double RadiusMax { get; set; }
		public int Convection { get; set; }
		public double InitialTemperature { get; set; }
		public double OuterTemperature { get; set; }
		public int ProcessTime { get; set; }
		public int CP { get; set; }
		public int Conduction { get; set; }
		public int Density { get; set; }

		public void PrototypeInit() {
			RadiusMin = 0;
			RadiusMax = 0.08;
			Convection = 300;
			InitialTemperature = 100;
			OuterTemperature = 200;
			CP = 700;
			Density = 7800;
			Conduction = 25;
			ProcessTime = 1000;
		}

		struct Node {
			public double XCoord { get; set; }
			public double Temperature { get; set; }
		}

		struct CalcPoint {
			public double Coord { get; set; }
			public double Weight { get; set; }
		}

		public string PrototypeMethod() {
			PrototypeInit();
			int nodesAmount = 9;
			int elementsAmount = nodesAmount - 1;
			CalcPoint[] calc3Points = new CalcPoint[]{
				new CalcPoint(){
					Weight = 0.5555555555,
					Coord = -0.7746
				},
				new CalcPoint(){
					Weight = 0.8888888888,
					Coord = 0
				},
				new CalcPoint(){
					Weight = 0.5555555555,
					Coord = 0.7746
				}
			};
			CalcPoint[] calc2Points = new CalcPoint[]{
				new CalcPoint(){
					Weight = 1,
					Coord = -0.5773502692
				},
				new CalcPoint(){
					Weight = 1,
					Coord = 0.5773502692
				}
			};
			CalcPoint []calcPoints = calc2Points;
			double[] N1 = new double[] { 0.5 * (1.0 - calcPoints[0].Coord), 0.5 * (1.0 - calcPoints[calcPoints.Length - 1].Coord) };
			double[] N2 = new double[] { 0.5 * (1.0 + calcPoints[0].Coord), 0.5 * (1.0 + calcPoints[calcPoints.Length - 1].Coord) };
			double deltaR = (RadiusMax - RadiusMin) / elementsAmount;
			double deltaTime = 50;
			int nTime = 8;
			Node[] nodes = new Node[nodesAmount];

			double[] globalF = new double[nodesAmount];
			double[,] globalK = new double[nodesAmount, nodesAmount];

			//Build Grid
			double xCoord = 0;
			for (int i = 0; i < nodesAmount; i++) {
				nodes[i].XCoord = Math.Round(xCoord, 6);
				nodes[i].Temperature = InitialTemperature;
				xCoord += deltaR;
			}

			double LocalConvection, calcPointR, calcPointTemp;
			double[,] localK = new double[2, 2];
			double[] localF = new double[2];
			StringBuilder sb = new StringBuilder();
			double [] temperatures;
			for (int iTime = 0; iTime < nTime; iTime++) {
				for (int iElement = 0; iElement < elementsAmount; iElement++) {
					LocalConvection = 0;
					if (iElement == elementsAmount - 1) {
						LocalConvection = Convection;
					}
					localK = new double[2, 2];
					localF = new double[2];
					double fModifier = (double)(CP * Density) * deltaR / deltaTime;
					for (int iPoint = 0; iPoint < calcPoints.Length; iPoint++) {
						calcPointR = N1[iPoint] * nodes[iElement].XCoord + N2[iPoint] * nodes[iElement + 1].XCoord;
						calcPointTemp = N1[iPoint] * nodes[iElement].Temperature + N2[iPoint] * nodes[iElement + 1].Temperature;

						localK[0, 0] += ((double)Conduction / deltaR) * calcPointR * calcPoints[iPoint].Weight + ((double)CP * Density * deltaR / deltaTime) * calcPointR * calcPoints[iPoint].Weight * N1[iPoint] * N1[iPoint];
						localK[0, 1] += -((double)Conduction / deltaR) * calcPointR * calcPoints[iPoint].Weight + ((double)CP * Density * deltaR / deltaTime) * calcPointR * calcPoints[iPoint].Weight * N1[iPoint] * N2[iPoint];
						localK[1, 0] = localK[0, 1];
						localK[1, 1] += ((double)Conduction / deltaR) * calcPointR * calcPoints[iPoint].Weight + ((double)CP * Density * deltaR / deltaTime) * calcPointR * calcPoints[iPoint].Weight * N2[iPoint] * N2[iPoint];
						localF[0] += calcPointTemp * N1[iPoint] * calcPointR * calcPoints[iPoint].Weight;
						localF[1] += calcPointTemp * N2[iPoint] * calcPointR * calcPoints[iPoint].Weight;
					}
					localF[0] *= fModifier;
					localF[1] *= fModifier;
					if (LocalConvection != 0) {
						localK[1, 1] += 2 * LocalConvection * RadiusMax;
						localF[1] += 2 * LocalConvection * RadiusMax * OuterTemperature;
					}
					localKToGlobalK(globalK, localK, iElement);
					localFToGlobalF(globalF, localF, iElement);
					
					for (int i = 0; i < elementsAmount; i++) {
						for (int j = 0; j < elementsAmount; j++) {
							//Console.Write(Math.Round(globalK[i, j]) + " ");
						}
						//Console.WriteLine();
					}
				}
				temperatures = MatrixOperation.GaussJordanReduction(globalK, globalF);
				for (int i = 0; i < temperatures.Length; i++) {
					nodes[i].Temperature = temperatures[i];
				}

				sb.Append(String.Format("\n\nTime step {0}:\n", iTime + 1));
				sb.Append(String.Join(" | ", nodes.Select(node => Math.Round(node.Temperature, 2))));
			}
			return sb.ToString();
		}

		private void localKToGlobalK(double[,] global, double[,] local, int index) {
			global[index, index] += local[0, 0];
			global[index + 1, index + 1] += local[1, 1];
			global[index, index + 1] += local[0, 1];
			global[index + 1, index] += local[1, 0];
		}

		private void localFToGlobalF(double[] global, double[] local, int index) {
			global[index] += local[0];
			global[index + 1] += local[1];
		}

		public void Solve(SolveMethod solveMethod) {
			throw new NotImplementedException();
		}

		public double[] Temperatures {
			get { throw new NotImplementedException(); }
		}
	}
}
