﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixOperations;
using NLog;

namespace TemperatureDistribution {
	public enum BoundaryCondition { NONE, CONVECTION, EXTERNAL_HEAT };

	public class LinearTemperatureDistribution : ITemperatureDistribution {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		public Grid Grid = new Grid();
		private double[,] globalCoefficients;
		private double[] boundaryConditions;
		public double OuterTemperature { get; set; }
		public double[] Temperatures { private set; get; }
		public double TotalLength { get; set; }
		public double Surface { get; set; }
		public double HeatFlux { get; set; }
		public double ConvectionCoefficient { get; set; }
		public double ConductionCoefficient { get; set; }
		public int NodesAmount { get; set; }
		public SolveMethod SolveMethod = SolveMethod.SAFE_CODE;

		private void Init() {
			globalCoefficients = new double[NodesAmount, NodesAmount];
			boundaryConditions = new double[NodesAmount];
			Temperatures = new double[NodesAmount];
			int elementsAmount = NodesAmount - 1;
			Grid.Elements = new Element[elementsAmount];
			double elementLength = TotalLength / elementsAmount;
			for (uint i = 0; i < Grid.Elements.Length; ++i) {
				Grid.Elements[i] = new Element {
					PointA = i,
					PointB = i + 1,
					Length = elementLength,
					Surface = this.Surface,
					Conduction = ConductionCoefficient,
					Convection = ConvectionCoefficient
				};
			}
			Grid.Nodes = new Node[NodesAmount];
			for (uint i = 0; i < NodesAmount; ++i) {
				Grid.Nodes[i] = new Node {
					ID = i,
					BoundaryCondition = BoundaryCondition.NONE,
					X = i * elementLength
				};
			}
		}

		private void SetBoundaryConditions() {
			Element lastElement = Grid.Elements.Last();
			Grid.Nodes[0].BoundaryCondition = BoundaryCondition.EXTERNAL_HEAT;
			Grid.Nodes[Grid.Nodes.Length - 1].BoundaryCondition = BoundaryCondition.CONVECTION;
			boundaryConditions[0] = -HeatFlux;
			boundaryConditions[boundaryConditions.Length - 1] = lastElement.Convection * lastElement.Surface * OuterTemperature;
		}

		private void SetGlobalCoefficients() {
			double[,] local;
			double Temp_C;
			for (int i = 0; i < Grid.Elements.Length; ++i) {
				Temp_C = (Grid.Elements[i].Surface * ConductionCoefficient) / Grid.Elements[i].Length;
				local = new double[,]{
					{Temp_C, -Temp_C},
					{-Temp_C, Temp_C}
				};
				for (int j = 0; j < 4; j++) {
					//local to global
					globalCoefficients[Grid.Elements[i].PointA + j / 2, Grid.Elements[i].PointA + j % 2] += local[j / 2, j % 2];
				}
			}
			//TODO: iterate elements and set boundary conditions
			globalCoefficients[globalCoefficients.GetLength(0) - 1, globalCoefficients.GetLength(0) - 1] += ConvectionCoefficient * Surface;
		}

		public void Solve(SolveMethod solveMethod = SolveMethod.SAFE_CODE) {
			Init();
			SetBoundaryConditions();
			SetGlobalCoefficients();
			switch (solveMethod) {
				case SolveMethod.SAFE_CODE:
					Temperatures = MatrixOperation.GaussJordanReduction(globalCoefficients, boundaryConditions, CalculateMethod.SAFE_CODE);
					break;
				case SolveMethod.UNSAFE_CODE:
					Temperatures = MatrixOperation.GaussJordanReduction(globalCoefficients, boundaryConditions, CalculateMethod.UNSAFE_CODE);
					break;
				default:
					throw new ArgumentException("Incorrect solve method supplied");
			}
		}
	}
}
