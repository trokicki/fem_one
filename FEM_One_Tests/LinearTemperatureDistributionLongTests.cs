﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TemperatureDistribution;

namespace LinearTemperatureDistributionTests
{
	[TestFixture]
    public class TemperatureDistribution1DTests{
		[SetUp]
		public void SetUp() {

		}

		[Test]
		public void Basic800ElementsSafeCodeTest() {
			LinearTemperatureDistribution temperatureDistribution1D = new LinearTemperatureDistribution() {
				OuterTemperature = 40,
				NodesAmount = 801,
				TotalLength = 7.5,
				Surface = 1,
				HeatFlux = -150,
				ConvectionCoefficient = 10,
				ConductionCoefficient = 75
			};
			temperatureDistribution1D.Solve(SolveMethod.SAFE_CODE);
			Assert.That(Math.Round(temperatureDistribution1D.Temperatures[0], 2), Is.EqualTo(70).Within(0.1));
			Assert.That(Math.Round(temperatureDistribution1D.Temperatures[temperatureDistribution1D.NodesAmount - 1], 2), Is.EqualTo(55).Within(0.1));
		}

		[Test]
		public void Basic800ElementsUnsafeCodeTest() {
			LinearTemperatureDistribution temperatureDistribution1D = new LinearTemperatureDistribution() {
				OuterTemperature = 40,
				NodesAmount = 801,
				TotalLength = 7.5,
				Surface = 1,
				HeatFlux = -150,
				ConvectionCoefficient = 10,
				ConductionCoefficient = 75
			};
			temperatureDistribution1D.Solve(SolveMethod.UNSAFE_CODE);
			Assert.That(Math.Round(temperatureDistribution1D.Temperatures[0], 2), Is.EqualTo(70).Within(0.1));
			Assert.That(Math.Round(temperatureDistribution1D.Temperatures[temperatureDistribution1D.NodesAmount - 1], 2), Is.EqualTo(55).Within(0.1));
		}

		[TearDown]
		public void TearDown() {

		}
    }
}
