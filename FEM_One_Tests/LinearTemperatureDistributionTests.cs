﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TemperatureDistribution;

namespace LinearTemperatureDistributionTests
{
	[TestFixture]
    public class LinearTemperatureDistributionTests{
		[SetUp]
		public void SetUp() {

		}

		[Test]
		public void Basic2ElementsTest() {
			decimal[] correctValues = new decimal[] { 70, 62.5m, 55 };
			LinearTemperatureDistribution temperatureDistribution1D = new LinearTemperatureDistribution(){
				OuterTemperature = 40,
				NodesAmount= 3,
				TotalLength= 7.5,
				Surface= 1,
				HeatFlux= -150,
				ConvectionCoefficient= 10,
				ConductionCoefficient= 75
			};
			temperatureDistribution1D.Solve();
			Assert.That(temperatureDistribution1D.Temperatures, Is.EqualTo(correctValues));
		}

		[Test]
		public void Basic8ElementsSafeCodeTest() {
			decimal[] correctValues = new decimal[] { 70, 68.125m, 66.25m, 64.375m, 62.5m, 60.625m, 58.75m, 56.875m, 55 };
			LinearTemperatureDistribution temperatureDistribution1D = new LinearTemperatureDistribution() {
				OuterTemperature = 40,
				NodesAmount = 9,
				TotalLength = 7.5,
				Surface = 1,
				HeatFlux = -150,
				ConvectionCoefficient = 10,
				ConductionCoefficient = 75
			};
			temperatureDistribution1D.Solve(SolveMethod.SAFE_CODE);
			Assert.That(temperatureDistribution1D.Temperatures, Is.EqualTo(correctValues));
		}

		[Test]
		public void Basic8ElementsUnsafeCodeTest() {
			decimal[] correctValues = new decimal[] { 70, 68.125m, 66.25m, 64.375m, 62.5m, 60.625m, 58.75m, 56.875m, 55 };
			LinearTemperatureDistribution temperatureDistribution1D = new LinearTemperatureDistribution() {
				OuterTemperature = 40,
				NodesAmount = 9,
				TotalLength = 7.5,
				Surface = 1,
				HeatFlux = -150,
				ConvectionCoefficient = 10,
				ConductionCoefficient = 75
			};
			temperatureDistribution1D.Solve(SolveMethod.UNSAFE_CODE);
			Assert.That(temperatureDistribution1D.Temperatures, Is.EqualTo(correctValues));
		}

		[Test]
		public void RandomElementsAmountTest() {
			int nodesAmount = (new Random()).Next(5, 80);
			LinearTemperatureDistribution temperatureDistribution1D = new LinearTemperatureDistribution() {
				OuterTemperature = 40,
				NodesAmount = nodesAmount,
				TotalLength = 7.5,
				Surface = 1,
				HeatFlux = -150,
				ConvectionCoefficient = 10,
				ConductionCoefficient = 75
			};
			temperatureDistribution1D.Solve();
			Assert.That(Math.Round(temperatureDistribution1D.Temperatures[0], 2), Is.EqualTo(70).Within(0.1));
			Assert.That(Math.Round(temperatureDistribution1D.Temperatures[temperatureDistribution1D.NodesAmount-1], 2), Is.EqualTo(55).Within(0.1));
		}

		[TearDown]
		public void TearDown() {

		}
    }
}
