﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TemperatureDistribution;

namespace RadialTemperatureDistributionTests {
	[TestFixture]
	public class RadialTemperatureDistributionTests {
		[SetUp]
		public void SetUp() {

		}

		[Test]
		public void PrototypeTest() {
			RadialTemperatureDistribution radialTemperatureDistribution = new RadialTemperatureDistribution();
			radialTemperatureDistribution.PrototypeInit();
			radialTemperatureDistribution.PrototypeMethod();
		}

		[TearDown]
		public void TearDown() {

		}
	}
}
