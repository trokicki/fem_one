﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TemperatureDistribution;
using Graph = System.Windows.Forms.DataVisualization.Charting;
using System.IO;

namespace FEM_One {
	public partial class MainForm : Form {
		private CultureInfo usCulture = new CultureInfo("en-US");

		public MainForm() {
			InitializeComponent();
			SetSingleElementSize();
		}

		private void SetSingleElementSize() {
			double singleElementSize = Math.Round(Convert.ToDouble(TxtTotalLength.Text, usCulture) / (double)NUDElementsAmount.Value, 3);
			TxtSingleElementSize.Text = singleElementSize.ToString();
		}

		private string TemperaturesToString(double[] temperatures) {
			Parallel.For(0, temperatures.Length, (i) => temperatures[i] = Math.Round(temperatures[i], 2));
			return String.Join(" | ", temperatures);
		}

		private string CalculatingReport(double[] temperatures, TimeSpan elapsed) {
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("Initial conditions: ");
			sb.AppendLine(String.Format("Surface: {0} m²", TxtSurface.Text));
			sb.AppendLine(String.Format("Convection: {0} W/m²K", TxtConvectionParam.Text));
			sb.AppendLine(String.Format("Conduction: {0} W/mK", TxtConductionParam.Text));
			sb.AppendLine(String.Format("Heat flux: {0} W/m²", TxtHeatFlux.Text));
			sb.AppendLine(String.Format("Outer temperature: {0} °C", TxtOuterTemperature.Text));
			sb.AppendLine(String.Format("Number of elements: {0}", NUDElementsAmount.Value.ToString()));
			sb.AppendLine(String.Format("Total length: {0} m", TxtTotalLength.Text));
			sb.AppendLine(String.Format("Single element's length: {0} m", TxtSingleElementSize.Text));
			sb.AppendLine();
			for (int i = 0; i < temperatures.Length; ++i) {
				sb.AppendLine(String.Format("{0}. {1}", i, Math.Round(temperatures[i], 2)));
			}
			sb.AppendLine();
			sb.AppendLine(String.Format("Calculating time: {0}", elapsed));
			sb.AppendLine();
			return sb.ToString();
		}

		private bool ResultsFieldToFile(string filePath = null) {
			DateTime currentTime = DateTime.Now;
			if (String.IsNullOrEmpty(filePath)) {
				string directory = "Results";
				if (!Directory.Exists(directory)) {
					Directory.CreateDirectory(directory);
				}
				filePath = Path.Combine(directory, String.Format("Temperatures_{0}.txt", currentTime.ToFileTime()));
			}
			
			using (StreamWriter writer = new StreamWriter(filePath)) {
				writer.WriteLine(String.Format("File generated {0}", currentTime));
				writer.WriteLine("---------------------------------------------");
				writer.Write(LblFullReport.Text);
			}
			if(File.Exists(filePath))
				return true;
			return false;
		}

		private void NUDElementsAmount_ValueChanged(object sender, EventArgs e) {
			SetSingleElementSize();
		}

		private void BtnCalculate_Click(object sender, EventArgs e) {
			SetStatusText("Calculating...");
			Cursor.Current = Cursors.WaitCursor;
			Stopwatch stopwatch = Stopwatch.StartNew();
			LinearTemperatureDistribution temperatureDistribution1D = InitTemperatureDistribution();
			if (RadioCalculatingMethod_UnsafeCode.Checked) {
				temperatureDistribution1D.Solve(SolveMethod.UNSAFE_CODE);
			}else{
				temperatureDistribution1D.Solve(SolveMethod.SAFE_CODE);
			}
			stopwatch.Stop();
			double[] temperatures = temperatureDistribution1D.Temperatures;
			Cursor.Current = Cursors.Default;
			LblFullReport.Text = CalculatingReport(temperatures, stopwatch.Elapsed);
			SetTemperaturesResultsText(temperatures);
			DrawChart(temperatures);
			VisualizeTemperature(temperatures);
			SetStatusText(stopwatch);
		}

		private void VisualizeTemperature(double[] temperatures) {
			int maxWidth = PanelDraw.Width;
			int maxHeight = PanelDraw.Height - 3;
			double maxTemperature = temperatures.Max();
			double minTemperature = temperatures.Min();
			double horizontalStep = (double)maxWidth / temperatures.Length;
			double valueStep = 1.0 / (maxTemperature - minTemperature);
			using (Graphics g = PanelDraw.CreateGraphics()) {
				Size size = new Size((int)horizontalStep, maxHeight);
				for (int i = 0; i < temperatures.Length; i++) {
					double value = (temperatures[i] - minTemperature) * valueStep;
					Color fillColor = getVisualizationColor(value);
					Point beginPoint = new Point((int)(i * (horizontalStep - 1)), 0);
					Rectangle rectangle = new Rectangle(beginPoint, size);
					Pen pen = new Pen(new SolidBrush(Color.FromArgb(0, 0, 0, 0)));
					g.DrawRectangle(pen, rectangle);
					g.FillRectangle(new SolidBrush(fillColor), rectangle);
				}
			}
		}

		private Color getVisualizationColor(double value) {
			return Color.FromArgb(255, (int)((1.0 - value) * 255), 0);
		}

		private void SetStatusText(Stopwatch stopwatch) {
			SetStatusText(String.Format("Execution time: {0} | Declared memory [KB]: {1}", stopwatch.Elapsed, Process.GetCurrentProcess().PrivateMemorySize64 / 1024));
		}

		private void SetStatusText(string text) {
			LblStatus.Text = text;
		}

		private void SetTemperaturesResultsText(double[] temperatures) {
			if (temperatures.Length < 64)
				LblResults.Text = TemperaturesToString(temperatures);
			else
				LblResults.Text = "See Results tab.";
		}

		private LinearTemperatureDistribution InitTemperatureDistribution() {
			return new LinearTemperatureDistribution() {
				OuterTemperature = Convert.ToDouble(TxtOuterTemperature.Text, usCulture),
				NodesAmount = (int)NUDElementsAmount.Value + 1,
				TotalLength = Convert.ToDouble(TxtTotalLength.Text, usCulture),
				Surface = Convert.ToDouble(TxtSurface.Text, usCulture),
				HeatFlux = Convert.ToDouble(TxtHeatFlux.Text, usCulture),
				ConvectionCoefficient = Convert.ToDouble(TxtConvectionParam.Text, usCulture),
				ConductionCoefficient = Convert.ToDouble(TxtConductionParam.Text, usCulture)
			};
		}

		private void DrawChart(double[] temperatures) {
			ChartTemperature.ChartAreas["draw"].AxisX.Minimum = 0;
			ChartTemperature.ChartAreas["draw"].AxisX.Interval = Convert.ToDouble(TxtSingleElementSize.Text);
			ChartTemperature.ChartAreas["draw"].AxisX.Maximum = Convert.ToDouble(TxtTotalLength.Text, usCulture);

			ChartTemperature.Series["Temperature"].ChartType = Graph.SeriesChartType.Line;
			ChartTemperature.Series["Temperature"].BorderWidth = 2;
			ChartTemperature.Series["Temperature"].MarkerStyle = Graph.MarkerStyle.Circle;
			ChartTemperature.Series["Temperature"].MarkerSize = 6;
			ChartTemperature.Series["Temperature"].ToolTip = "Temperature distribution";
			ChartTemperature.Series["Temperature"].Points.Clear();
			double xStep = ChartTemperature.ChartAreas["draw"].AxisX.Maximum / (temperatures.Length - 1);
			for (int i = 0; i < temperatures.Length; i++)
			{
				ChartTemperature.Series["Temperature"].Points.AddXY(i * xStep, temperatures[i]);
			}				
		}

		private void TxtTotalLength_TextChanged(object sender, EventArgs e) {
			SetSingleElementSize();
		}

		private void BtnSaveResultsToFile_Click(object sender, EventArgs e) {
			SaveResultsFileDialog.ShowDialog();
		}

		private void SaveResultsFileDialog_FileOk(object sender, CancelEventArgs e) {
			string fileName = SaveResultsFileDialog.FileName;
			if (ResultsFieldToFile(fileName)) {
				MessageBox.Show("File saved", "Save results", MessageBoxButtons.OK);
			}
		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
			AboutBox aboutBox = new AboutBox();
			aboutBox.ShowDialog();
		}

		private void button1_Click(object sender, EventArgs e) {
			RadialTemperatureDistribution radialTemperatureDistribution = new RadialTemperatureDistribution();
			radialTemperatureDistribution.PrototypeInit();
			
			string output = radialTemperatureDistribution.PrototypeMethod();
			Console.WriteLine(output);
			LblFullReport.Text = output; 
		}
	}
}
