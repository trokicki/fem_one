﻿namespace FEM_One {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.RadialTemperatureDistributionTab = new System.Windows.Forms.TabPage();
			this.button1 = new System.Windows.Forms.Button();
			this.GroupRadialDistributionConditions = new System.Windows.Forms.GroupBox();
			this.label20 = new System.Windows.Forms.Label();
			this.label35 = new System.Windows.Forms.Label();
			this.TxtRadialConduction = new System.Windows.Forms.TextBox();
			this.label33 = new System.Windows.Forms.Label();
			this.label36 = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.TxtRadialProcessTime = new System.Windows.Forms.TextBox();
			this.label31 = new System.Windows.Forms.Label();
			this.label32 = new System.Windows.Forms.Label();
			this.TxtRadialInitialTemperature = new System.Windows.Forms.TextBox();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.TxtRadialOuterTemperature = new System.Windows.Forms.TextBox();
			this.label27 = new System.Windows.Forms.Label();
			this.TxtRadialConvection = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.TxtRadialDensity = new System.Windows.Forms.TextBox();
			this.label23 = new System.Windows.Forms.Label();
			this.TxtRadiusMax = new System.Windows.Forms.TextBox();
			this.label24 = new System.Windows.Forms.Label();
			this.TxtRadiusMin = new System.Windows.Forms.TextBox();
			this.label25 = new System.Windows.Forms.Label();
			this.TxtCP = new System.Windows.Forms.TextBox();
			this.LinearTemperatureDistributionTab = new System.Windows.Forms.TabPage();
			this.ChartTemperature = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.LblStatus = new System.Windows.Forms.Label();
			this.BtnCalculate = new System.Windows.Forms.Button();
			this.LblResults = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.GroupMeshParams = new System.Windows.Forms.GroupBox();
			this.NUDElementsAmount = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.TxtSingleElementSize = new System.Windows.Forms.TextBox();
			this.chbConstantElementsSize = new System.Windows.Forms.CheckBox();
			this.TxtTotalLength = new System.Windows.Forms.TextBox();
			this.GroupConditions = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.TxtConductionParam = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.TxtConvectionParam = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.TxtSurface = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.TxtHeatFlux = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.TxtOuterTemperature = new System.Windows.Forms.TextBox();
			this.SettingsTab = new System.Windows.Forms.TabPage();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.RadioCalculatingMethod_UnsafeCode = new System.Windows.Forms.RadioButton();
			this.RadioCalculatingMethod_SafeCode = new System.Windows.Forms.RadioButton();
			this.ResultsTab = new System.Windows.Forms.TabPage();
			this.BtnSaveResultsToFile = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.LblFullReport = new System.Windows.Forms.Label();
			this.SaveResultsFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.PanelDraw = new System.Windows.Forms.Panel();
			this.menuStrip1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.RadialTemperatureDistributionTab.SuspendLayout();
			this.GroupRadialDistributionConditions.SuspendLayout();
			this.LinearTemperatureDistributionTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ChartTemperature)).BeginInit();
			this.GroupMeshParams.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.NUDElementsAmount)).BeginInit();
			this.GroupConditions.SuspendLayout();
			this.SettingsTab.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.ResultsTab.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1064, 24);
			this.menuStrip1.TabIndex = 34;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.RadialTemperatureDistributionTab);
			this.tabControl1.Controls.Add(this.LinearTemperatureDistributionTab);
			this.tabControl1.Controls.Add(this.SettingsTab);
			this.tabControl1.Controls.Add(this.ResultsTab);
			this.tabControl1.Location = new System.Drawing.Point(0, 27);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1064, 726);
			this.tabControl1.TabIndex = 35;
			// 
			// RadialTemperatureDistributionTab
			// 
			this.RadialTemperatureDistributionTab.Controls.Add(this.button1);
			this.RadialTemperatureDistributionTab.Controls.Add(this.GroupRadialDistributionConditions);
			this.RadialTemperatureDistributionTab.Location = new System.Drawing.Point(4, 22);
			this.RadialTemperatureDistributionTab.Name = "RadialTemperatureDistributionTab";
			this.RadialTemperatureDistributionTab.Size = new System.Drawing.Size(1056, 700);
			this.RadialTemperatureDistributionTab.TabIndex = 3;
			this.RadialTemperatureDistributionTab.Text = "Radial temperature distribution";
			this.RadialTemperatureDistributionTab.UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 250);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(232, 45);
			this.button1.TabIndex = 37;
			this.button1.Text = "Calculate";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// GroupRadialDistributionConditions
			// 
			this.GroupRadialDistributionConditions.Controls.Add(this.label20);
			this.GroupRadialDistributionConditions.Controls.Add(this.label35);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtRadialConduction);
			this.GroupRadialDistributionConditions.Controls.Add(this.label33);
			this.GroupRadialDistributionConditions.Controls.Add(this.label36);
			this.GroupRadialDistributionConditions.Controls.Add(this.label34);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtRadialProcessTime);
			this.GroupRadialDistributionConditions.Controls.Add(this.label31);
			this.GroupRadialDistributionConditions.Controls.Add(this.label32);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtRadialInitialTemperature);
			this.GroupRadialDistributionConditions.Controls.Add(this.label29);
			this.GroupRadialDistributionConditions.Controls.Add(this.label30);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtRadialOuterTemperature);
			this.GroupRadialDistributionConditions.Controls.Add(this.label27);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtRadialConvection);
			this.GroupRadialDistributionConditions.Controls.Add(this.label17);
			this.GroupRadialDistributionConditions.Controls.Add(this.label18);
			this.GroupRadialDistributionConditions.Controls.Add(this.label19);
			this.GroupRadialDistributionConditions.Controls.Add(this.label21);
			this.GroupRadialDistributionConditions.Controls.Add(this.label22);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtRadialDensity);
			this.GroupRadialDistributionConditions.Controls.Add(this.label23);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtRadiusMax);
			this.GroupRadialDistributionConditions.Controls.Add(this.label24);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtRadiusMin);
			this.GroupRadialDistributionConditions.Controls.Add(this.label25);
			this.GroupRadialDistributionConditions.Controls.Add(this.TxtCP);
			this.GroupRadialDistributionConditions.Location = new System.Drawing.Point(8, 3);
			this.GroupRadialDistributionConditions.Name = "GroupRadialDistributionConditions";
			this.GroupRadialDistributionConditions.Size = new System.Drawing.Size(232, 241);
			this.GroupRadialDistributionConditions.TabIndex = 36;
			this.GroupRadialDistributionConditions.TabStop = false;
			this.GroupRadialDistributionConditions.Text = "Conditions";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(166, 66);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(41, 13);
			this.label20.TabIndex = 40;
			this.label20.Text = "W/m²K";
			// 
			// label35
			// 
			this.label35.AutoSize = true;
			this.label35.Location = new System.Drawing.Point(46, 185);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(79, 13);
			this.label35.TabIndex = 38;
			this.label35.Text = "Conduction (k):";
			// 
			// TxtRadialConduction
			// 
			this.TxtRadialConduction.Location = new System.Drawing.Point(127, 182);
			this.TxtRadialConduction.Name = "TxtRadialConduction";
			this.TxtRadialConduction.Size = new System.Drawing.Size(41, 20);
			this.TxtRadialConduction.TabIndex = 37;
			this.TxtRadialConduction.Text = "25";
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Location = new System.Drawing.Point(168, 142);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(12, 13);
			this.label33.TabIndex = 37;
			this.label33.Text = "s";
			// 
			// label36
			// 
			this.label36.AutoSize = true;
			this.label36.Location = new System.Drawing.Point(170, 185);
			this.label36.Name = "label36";
			this.label36.Size = new System.Drawing.Size(38, 13);
			this.label36.TabIndex = 39;
			this.label36.Text = "W/mK";
			// 
			// label34
			// 
			this.label34.AutoSize = true;
			this.label34.Location = new System.Drawing.Point(52, 138);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(70, 13);
			this.label34.TabIndex = 36;
			this.label34.Text = "Process time:";
			// 
			// TxtRadialProcessTime
			// 
			this.TxtRadialProcessTime.Location = new System.Drawing.Point(125, 135);
			this.TxtRadialProcessTime.Name = "TxtRadialProcessTime";
			this.TxtRadialProcessTime.Size = new System.Drawing.Size(41, 20);
			this.TxtRadialProcessTime.TabIndex = 35;
			this.TxtRadialProcessTime.Text = "1000";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Location = new System.Drawing.Point(167, 117);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(18, 13);
			this.label31.TabIndex = 34;
			this.label31.Text = "°C";
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Location = new System.Drawing.Point(28, 113);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(93, 13);
			this.label32.TabIndex = 33;
			this.label32.Text = "Initial temperature:";
			// 
			// TxtRadialInitialTemperature
			// 
			this.TxtRadialInitialTemperature.Location = new System.Drawing.Point(124, 110);
			this.TxtRadialInitialTemperature.Name = "TxtRadialInitialTemperature";
			this.TxtRadialInitialTemperature.Size = new System.Drawing.Size(41, 20);
			this.TxtRadialInitialTemperature.TabIndex = 32;
			this.TxtRadialInitialTemperature.Text = "100";
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Location = new System.Drawing.Point(167, 94);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(18, 13);
			this.label29.TabIndex = 31;
			this.label29.Text = "°C";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Location = new System.Drawing.Point(27, 90);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(95, 13);
			this.label30.TabIndex = 30;
			this.label30.Text = "Outer temperature:";
			// 
			// TxtRadialOuterTemperature
			// 
			this.TxtRadialOuterTemperature.Location = new System.Drawing.Point(124, 87);
			this.TxtRadialOuterTemperature.Name = "TxtRadialOuterTemperature";
			this.TxtRadialOuterTemperature.Size = new System.Drawing.Size(41, 20);
			this.TxtRadialOuterTemperature.TabIndex = 29;
			this.TxtRadialOuterTemperature.Text = "1200";
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(40, 66);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(80, 13);
			this.label27.TabIndex = 28;
			this.label27.Text = "Convection (α):";
			// 
			// TxtRadialConvection
			// 
			this.TxtRadialConvection.Location = new System.Drawing.Point(123, 63);
			this.TxtRadialConvection.Name = "TxtRadialConvection";
			this.TxtRadialConvection.Size = new System.Drawing.Size(41, 20);
			this.TxtRadialConvection.TabIndex = 25;
			this.TxtRadialConvection.Text = "300";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(53, 16);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(66, 13);
			this.label17.TabIndex = 3;
			this.label17.Text = "Radius MIN:";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(170, 162);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(46, 13);
			this.label18.TabIndex = 24;
			this.label18.Text = "J/(kg*K)";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(100, 160);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(23, 13);
			this.label19.TabIndex = 0;
			this.label19.Text = "Cp:";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(51, 41);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(69, 13);
			this.label21.TabIndex = 4;
			this.label21.Text = "Radius MAX:";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(73, 210);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(45, 13);
			this.label22.TabIndex = 5;
			this.label22.Text = "Density:";
			// 
			// TxtRadialDensity
			// 
			this.TxtRadialDensity.Location = new System.Drawing.Point(127, 208);
			this.TxtRadialDensity.Name = "TxtRadialDensity";
			this.TxtRadialDensity.Size = new System.Drawing.Size(41, 20);
			this.TxtRadialDensity.TabIndex = 3;
			this.TxtRadialDensity.Text = "7800";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(170, 211);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(38, 13);
			this.label23.TabIndex = 20;
			this.label23.Text = "kg/m3";
			// 
			// TxtRadiusMax
			// 
			this.TxtRadiusMax.Location = new System.Drawing.Point(123, 38);
			this.TxtRadiusMax.Name = "TxtRadiusMax";
			this.TxtRadiusMax.Size = new System.Drawing.Size(41, 20);
			this.TxtRadiusMax.TabIndex = 2;
			this.TxtRadiusMax.Text = "0.05";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(166, 41);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(15, 13);
			this.label24.TabIndex = 19;
			this.label24.Text = "m";
			// 
			// TxtRadiusMin
			// 
			this.TxtRadiusMin.Location = new System.Drawing.Point(123, 12);
			this.TxtRadiusMin.Name = "TxtRadiusMin";
			this.TxtRadiusMin.Size = new System.Drawing.Size(41, 20);
			this.TxtRadiusMin.TabIndex = 1;
			this.TxtRadiusMin.Text = "0";
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(166, 16);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(15, 13);
			this.label25.TabIndex = 18;
			this.label25.Text = "m";
			// 
			// TxtCP
			// 
			this.TxtCP.Location = new System.Drawing.Point(126, 158);
			this.TxtCP.Name = "TxtCP";
			this.TxtCP.Size = new System.Drawing.Size(41, 20);
			this.TxtCP.TabIndex = 4;
			this.TxtCP.Text = "700";
			// 
			// LinearTemperatureDistributionTab
			// 
			this.LinearTemperatureDistributionTab.Controls.Add(this.PanelDraw);
			this.LinearTemperatureDistributionTab.Controls.Add(this.ChartTemperature);
			this.LinearTemperatureDistributionTab.Controls.Add(this.LblStatus);
			this.LinearTemperatureDistributionTab.Controls.Add(this.BtnCalculate);
			this.LinearTemperatureDistributionTab.Controls.Add(this.LblResults);
			this.LinearTemperatureDistributionTab.Controls.Add(this.label16);
			this.LinearTemperatureDistributionTab.Controls.Add(this.GroupMeshParams);
			this.LinearTemperatureDistributionTab.Controls.Add(this.GroupConditions);
			this.LinearTemperatureDistributionTab.Location = new System.Drawing.Point(4, 22);
			this.LinearTemperatureDistributionTab.Name = "LinearTemperatureDistributionTab";
			this.LinearTemperatureDistributionTab.Padding = new System.Windows.Forms.Padding(3);
			this.LinearTemperatureDistributionTab.Size = new System.Drawing.Size(1056, 700);
			this.LinearTemperatureDistributionTab.TabIndex = 0;
			this.LinearTemperatureDistributionTab.Text = "Linear temperature distribution";
			this.LinearTemperatureDistributionTab.UseVisualStyleBackColor = true;
			// 
			// ChartTemperature
			// 
			chartArea1.Name = "draw";
			this.ChartTemperature.ChartAreas.Add(chartArea1);
			legend1.Name = "Legend1";
			this.ChartTemperature.Legends.Add(legend1);
			this.ChartTemperature.Location = new System.Drawing.Point(9, 205);
			this.ChartTemperature.Name = "ChartTemperature";
			this.ChartTemperature.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.EarthTones;
			series1.ChartArea = "draw";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series1.IsVisibleInLegend = false;
			series1.Legend = "Legend1";
			series1.Name = "Temperature";
			series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
			series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
			this.ChartTemperature.Series.Add(series1);
			this.ChartTemperature.Size = new System.Drawing.Size(659, 497);
			this.ChartTemperature.TabIndex = 40;
			this.ChartTemperature.Text = "Temperature distribution";
			// 
			// LblStatus
			// 
			this.LblStatus.AutoSize = true;
			this.LblStatus.Location = new System.Drawing.Point(674, 681);
			this.LblStatus.Name = "LblStatus";
			this.LblStatus.Size = new System.Drawing.Size(29, 13);
			this.LblStatus.TabIndex = 39;
			this.LblStatus.Text = "[idle]";
			// 
			// BtnCalculate
			// 
			this.BtnCalculate.Location = new System.Drawing.Point(593, 118);
			this.BtnCalculate.Name = "BtnCalculate";
			this.BtnCalculate.Size = new System.Drawing.Size(75, 21);
			this.BtnCalculate.TabIndex = 34;
			this.BtnCalculate.Text = "Calculate";
			this.BtnCalculate.UseVisualStyleBackColor = true;
			this.BtnCalculate.Click += new System.EventHandler(this.BtnCalculate_Click);
			// 
			// LblResults
			// 
			this.LblResults.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.LblResults.Location = new System.Drawing.Point(696, 29);
			this.LblResults.Name = "LblResults";
			this.LblResults.Size = new System.Drawing.Size(352, 118);
			this.LblResults.TabIndex = 38;
			this.LblResults.Text = "[no results]";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(696, 7);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(75, 13);
			this.label16.TabIndex = 37;
			this.label16.Text = "Temperatures:";
			// 
			// GroupMeshParams
			// 
			this.GroupMeshParams.Controls.Add(this.NUDElementsAmount);
			this.GroupMeshParams.Controls.Add(this.label2);
			this.GroupMeshParams.Controls.Add(this.label13);
			this.GroupMeshParams.Controls.Add(this.label3);
			this.GroupMeshParams.Controls.Add(this.label12);
			this.GroupMeshParams.Controls.Add(this.label8);
			this.GroupMeshParams.Controls.Add(this.TxtSingleElementSize);
			this.GroupMeshParams.Controls.Add(this.chbConstantElementsSize);
			this.GroupMeshParams.Controls.Add(this.TxtTotalLength);
			this.GroupMeshParams.Location = new System.Drawing.Point(339, 7);
			this.GroupMeshParams.Name = "GroupMeshParams";
			this.GroupMeshParams.Size = new System.Drawing.Size(329, 103);
			this.GroupMeshParams.TabIndex = 36;
			this.GroupMeshParams.TabStop = false;
			this.GroupMeshParams.Text = "Mesh parameters";
			// 
			// NUDElementsAmount
			// 
			this.NUDElementsAmount.Location = new System.Drawing.Point(121, 19);
			this.NUDElementsAmount.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
			this.NUDElementsAmount.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.NUDElementsAmount.Name = "NUDElementsAmount";
			this.NUDElementsAmount.Size = new System.Drawing.Size(58, 20);
			this.NUDElementsAmount.TabIndex = 6;
			this.NUDElementsAmount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(0, 67);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(118, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Single element\'s length:";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(164, 68);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(15, 13);
			this.label13.TabIndex = 22;
			this.label13.Text = "m";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(36, 46);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(83, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Total length [m]:";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(164, 46);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(15, 13);
			this.label12.TabIndex = 21;
			this.label12.Text = "m";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(14, 22);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(104, 13);
			this.label8.TabIndex = 7;
			this.label8.Text = "Number of elements:";
			// 
			// TxtSingleElementSize
			// 
			this.TxtSingleElementSize.Location = new System.Drawing.Point(121, 64);
			this.TxtSingleElementSize.Name = "TxtSingleElementSize";
			this.TxtSingleElementSize.ReadOnly = true;
			this.TxtSingleElementSize.Size = new System.Drawing.Size(41, 20);
			this.TxtSingleElementSize.TabIndex = 8;
			// 
			// chbConstantElementsSize
			// 
			this.chbConstantElementsSize.AutoSize = true;
			this.chbConstantElementsSize.Checked = true;
			this.chbConstantElementsSize.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chbConstantElementsSize.Enabled = false;
			this.chbConstantElementsSize.Location = new System.Drawing.Point(185, 44);
			this.chbConstantElementsSize.Name = "chbConstantElementsSize";
			this.chbConstantElementsSize.Size = new System.Drawing.Size(133, 17);
			this.chbConstantElementsSize.TabIndex = 9;
			this.chbConstantElementsSize.Text = "constant elements size";
			this.chbConstantElementsSize.UseVisualStyleBackColor = true;
			// 
			// TxtTotalLength
			// 
			this.TxtTotalLength.Location = new System.Drawing.Point(121, 42);
			this.TxtTotalLength.Name = "TxtTotalLength";
			this.TxtTotalLength.Size = new System.Drawing.Size(41, 20);
			this.TxtTotalLength.TabIndex = 7;
			this.TxtTotalLength.Text = "7.5";
			// 
			// GroupConditions
			// 
			this.GroupConditions.Controls.Add(this.label4);
			this.GroupConditions.Controls.Add(this.label15);
			this.GroupConditions.Controls.Add(this.label1);
			this.GroupConditions.Controls.Add(this.label14);
			this.GroupConditions.Controls.Add(this.label5);
			this.GroupConditions.Controls.Add(this.label6);
			this.GroupConditions.Controls.Add(this.TxtConductionParam);
			this.GroupConditions.Controls.Add(this.label11);
			this.GroupConditions.Controls.Add(this.TxtConvectionParam);
			this.GroupConditions.Controls.Add(this.label10);
			this.GroupConditions.Controls.Add(this.TxtSurface);
			this.GroupConditions.Controls.Add(this.label9);
			this.GroupConditions.Controls.Add(this.TxtHeatFlux);
			this.GroupConditions.Controls.Add(this.label7);
			this.GroupConditions.Controls.Add(this.TxtOuterTemperature);
			this.GroupConditions.Location = new System.Drawing.Point(8, 6);
			this.GroupConditions.Name = "GroupConditions";
			this.GroupConditions.Size = new System.Drawing.Size(325, 140);
			this.GroupConditions.TabIndex = 35;
			this.GroupConditions.TabStop = false;
			this.GroupConditions.Text = "Conditions";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(74, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(47, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "Surface:";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(167, 87);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(34, 13);
			this.label15.TabIndex = 24;
			this.label15.Text = "W/m²";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(55, 83);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(67, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Heat flux (q):";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(167, 109);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(18, 13);
			this.label14.TabIndex = 23;
			this.label14.Text = "°C";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(41, 38);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "Convection (α):";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(42, 60);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(79, 13);
			this.label6.TabIndex = 5;
			this.label6.Text = "Conduction (k):";
			// 
			// TxtConductionParam
			// 
			this.TxtConductionParam.Location = new System.Drawing.Point(123, 57);
			this.TxtConductionParam.Name = "TxtConductionParam";
			this.TxtConductionParam.Size = new System.Drawing.Size(41, 20);
			this.TxtConductionParam.TabIndex = 3;
			this.TxtConductionParam.Text = "75";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(166, 60);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(38, 13);
			this.label11.TabIndex = 20;
			this.label11.Text = "W/mK";
			// 
			// TxtConvectionParam
			// 
			this.TxtConvectionParam.Location = new System.Drawing.Point(123, 35);
			this.TxtConvectionParam.Name = "TxtConvectionParam";
			this.TxtConvectionParam.Size = new System.Drawing.Size(41, 20);
			this.TxtConvectionParam.TabIndex = 2;
			this.TxtConvectionParam.Text = "10";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(166, 38);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(41, 13);
			this.label10.TabIndex = 19;
			this.label10.Text = "W/m²K";
			// 
			// TxtSurface
			// 
			this.TxtSurface.Location = new System.Drawing.Point(123, 12);
			this.TxtSurface.Name = "TxtSurface";
			this.TxtSurface.Size = new System.Drawing.Size(41, 20);
			this.TxtSurface.TabIndex = 1;
			this.TxtSurface.Text = "1";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(166, 16);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(18, 13);
			this.label9.TabIndex = 18;
			this.label9.Text = "m²";
			// 
			// TxtHeatFlux
			// 
			this.TxtHeatFlux.Location = new System.Drawing.Point(124, 80);
			this.TxtHeatFlux.Name = "TxtHeatFlux";
			this.TxtHeatFlux.Size = new System.Drawing.Size(41, 20);
			this.TxtHeatFlux.TabIndex = 4;
			this.TxtHeatFlux.Text = "-150";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(28, 105);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(95, 13);
			this.label7.TabIndex = 17;
			this.label7.Text = "Outer temperature:";
			// 
			// TxtOuterTemperature
			// 
			this.TxtOuterTemperature.Location = new System.Drawing.Point(124, 102);
			this.TxtOuterTemperature.Name = "TxtOuterTemperature";
			this.TxtOuterTemperature.Size = new System.Drawing.Size(41, 20);
			this.TxtOuterTemperature.TabIndex = 5;
			this.TxtOuterTemperature.Text = "40";
			// 
			// SettingsTab
			// 
			this.SettingsTab.Controls.Add(this.groupBox1);
			this.SettingsTab.Location = new System.Drawing.Point(4, 22);
			this.SettingsTab.Name = "SettingsTab";
			this.SettingsTab.Padding = new System.Windows.Forms.Padding(3);
			this.SettingsTab.Size = new System.Drawing.Size(1056, 700);
			this.SettingsTab.TabIndex = 1;
			this.SettingsTab.Text = "Common settings";
			this.SettingsTab.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.RadioCalculatingMethod_UnsafeCode);
			this.groupBox1.Controls.Add(this.RadioCalculatingMethod_SafeCode);
			this.groupBox1.Location = new System.Drawing.Point(6, 6);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(311, 100);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Calculating method";
			// 
			// RadioCalculatingMethod_UnsafeCode
			// 
			this.RadioCalculatingMethod_UnsafeCode.AutoSize = true;
			this.RadioCalculatingMethod_UnsafeCode.Location = new System.Drawing.Point(7, 44);
			this.RadioCalculatingMethod_UnsafeCode.Name = "RadioCalculatingMethod_UnsafeCode";
			this.RadioCalculatingMethod_UnsafeCode.Size = new System.Drawing.Size(300, 17);
			this.RadioCalculatingMethod_UnsafeCode.TabIndex = 1;
			this.RadioCalculatingMethod_UnsafeCode.Text = "C# Unsafe Code [unmanaged code, direct pointers usage]";
			this.RadioCalculatingMethod_UnsafeCode.UseVisualStyleBackColor = true;
			// 
			// RadioCalculatingMethod_SafeCode
			// 
			this.RadioCalculatingMethod_SafeCode.AutoSize = true;
			this.RadioCalculatingMethod_SafeCode.Checked = true;
			this.RadioCalculatingMethod_SafeCode.Location = new System.Drawing.Point(7, 20);
			this.RadioCalculatingMethod_SafeCode.Name = "RadioCalculatingMethod_SafeCode";
			this.RadioCalculatingMethod_SafeCode.Size = new System.Drawing.Size(169, 17);
			this.RadioCalculatingMethod_SafeCode.TabIndex = 0;
			this.RadioCalculatingMethod_SafeCode.TabStop = true;
			this.RadioCalculatingMethod_SafeCode.Text = "C# Safe Code [CLR managed]";
			this.RadioCalculatingMethod_SafeCode.UseVisualStyleBackColor = true;
			// 
			// ResultsTab
			// 
			this.ResultsTab.Controls.Add(this.BtnSaveResultsToFile);
			this.ResultsTab.Controls.Add(this.panel1);
			this.ResultsTab.Location = new System.Drawing.Point(4, 22);
			this.ResultsTab.Name = "ResultsTab";
			this.ResultsTab.Size = new System.Drawing.Size(1056, 700);
			this.ResultsTab.TabIndex = 2;
			this.ResultsTab.Text = "Results";
			this.ResultsTab.UseVisualStyleBackColor = true;
			// 
			// BtnSaveResultsToFile
			// 
			this.BtnSaveResultsToFile.Location = new System.Drawing.Point(946, 674);
			this.BtnSaveResultsToFile.Name = "BtnSaveResultsToFile";
			this.BtnSaveResultsToFile.Size = new System.Drawing.Size(107, 23);
			this.BtnSaveResultsToFile.TabIndex = 2;
			this.BtnSaveResultsToFile.Text = "Save results to file";
			this.BtnSaveResultsToFile.UseVisualStyleBackColor = true;
			this.BtnSaveResultsToFile.Click += new System.EventHandler(this.BtnSaveResultsToFile_Click);
			// 
			// panel1
			// 
			this.panel1.AutoScroll = true;
			this.panel1.Controls.Add(this.LblFullReport);
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1056, 668);
			this.panel1.TabIndex = 1;
			// 
			// LblFullReport
			// 
			this.LblFullReport.AutoSize = true;
			this.LblFullReport.Location = new System.Drawing.Point(12, 10);
			this.LblFullReport.MaximumSize = new System.Drawing.Size(1050, 0);
			this.LblFullReport.Name = "LblFullReport";
			this.LblFullReport.Size = new System.Drawing.Size(74, 13);
			this.LblFullReport.TabIndex = 0;
			this.LblFullReport.Text = "No results yet.";
			// 
			// SaveResultsFileDialog
			// 
			this.SaveResultsFileDialog.DefaultExt = "txt";
			this.SaveResultsFileDialog.Filter = "Text file|*.txt|All files|*.*";
			this.SaveResultsFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveResultsFileDialog_FileOk);
			// 
			// PanelDraw
			// 
			this.PanelDraw.Location = new System.Drawing.Point(9, 153);
			this.PanelDraw.Name = "PanelDraw";
			this.PanelDraw.Size = new System.Drawing.Size(659, 46);
			this.PanelDraw.TabIndex = 41;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1064, 752);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "Temperature Distribution";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.RadialTemperatureDistributionTab.ResumeLayout(false);
			this.GroupRadialDistributionConditions.ResumeLayout(false);
			this.GroupRadialDistributionConditions.PerformLayout();
			this.LinearTemperatureDistributionTab.ResumeLayout(false);
			this.LinearTemperatureDistributionTab.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ChartTemperature)).EndInit();
			this.GroupMeshParams.ResumeLayout(false);
			this.GroupMeshParams.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.NUDElementsAmount)).EndInit();
			this.GroupConditions.ResumeLayout(false);
			this.GroupConditions.PerformLayout();
			this.SettingsTab.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResultsTab.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage LinearTemperatureDistributionTab;
		private System.Windows.Forms.TabPage SettingsTab;
		private System.Windows.Forms.DataVisualization.Charting.Chart ChartTemperature;
		private System.Windows.Forms.Label LblStatus;
		private System.Windows.Forms.Button BtnCalculate;
		private System.Windows.Forms.Label LblResults;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.GroupBox GroupMeshParams;
		private System.Windows.Forms.NumericUpDown NUDElementsAmount;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox TxtSingleElementSize;
		private System.Windows.Forms.CheckBox chbConstantElementsSize;
		private System.Windows.Forms.TextBox TxtTotalLength;
		private System.Windows.Forms.GroupBox GroupConditions;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox TxtConductionParam;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox TxtConvectionParam;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox TxtSurface;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox TxtHeatFlux;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox TxtOuterTemperature;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton RadioCalculatingMethod_UnsafeCode;
		private System.Windows.Forms.RadioButton RadioCalculatingMethod_SafeCode;
		private System.Windows.Forms.TabPage ResultsTab;
		private System.Windows.Forms.Label LblFullReport;
		private System.Windows.Forms.Button BtnSaveResultsToFile;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.SaveFileDialog SaveResultsFileDialog;
		private System.Windows.Forms.TabPage RadialTemperatureDistributionTab;
		private System.Windows.Forms.GroupBox GroupRadialDistributionConditions;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox TxtRadialDensity;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.TextBox TxtRadiusMax;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox TxtRadiusMin;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.TextBox TxtCP;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox TxtRadialConvection;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.TextBox TxtRadialConduction;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.TextBox TxtRadialProcessTime;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.TextBox TxtRadialInitialTemperature;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.TextBox TxtRadialOuterTemperature;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel PanelDraw;
	}
}

